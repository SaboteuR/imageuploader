<?php

namespace ImageUploader\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 */
class RetryCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('retry')
            ->setDescription('Retries failed image processing jobs')
            ->addOption('number', null, InputOption::VALUE_OPTIONAL, 'Uploads given number of images and then stops')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $number = $input->getOption('number');

        $this->getContainer()->get('retrier')->run($number);
    }
}
