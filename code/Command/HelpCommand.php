<?php

namespace ImageUploader\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 */
class HelpCommand extends Command
{
    private $helpMessage = <<<HELP
Usage:

 command [arguments]

Available commands:

 <info>schedule</info> Add filenames to resize queue

 <info>resize</info> Resize next images from the queue

 <info>status</info> Output current status in format %queue%:%number_of_images%

 <info>upload<info> Upload next images to remote storage
HELP;

    protected function configure()
    {
        $this->setName('help')
            ->setDescription('Displays help');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln($this->helpMessage);
    }
}
