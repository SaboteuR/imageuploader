<?php

namespace ImageUploader\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 */
class ScheduleCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('schedule')
            ->setDescription('Add filenames to resize queue')
            ->addArgument('path', InputArgument::REQUIRED, 'Path to source images')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = $input->getArgument('path');
        
        $this->getContainer()->get('scheduler')->schedule($path);
    }
}
