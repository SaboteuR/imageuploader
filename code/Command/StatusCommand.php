<?php

namespace ImageUploader\Command;

use ImageUploader\Model\Stats;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 */
class StatusCommand extends ContainerAwareCommand
{
    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('status')
            ->setDescription('Shows current bot status')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Stats $stats */
        $stats = $this->getContainer()->get('stats_collector')->get();

        $output->writeln('Images Processor Bot');
        $table = new Table($output);
        $table
            ->setHeaders(['Queue', 'Count'])
            ->setRows([
                ['resize', $stats->getResizeCount()],
                ['upload', $stats->getUploadCount()],
                ['done', $stats->getDoneCount()],
                ['failed', $stats->getFailedCount()]
            ])
        ;
        $table->setStyle('compact');
        $table->render();
    }
}
