<?php

namespace ImageUploader\BatchSize;

use ImageUploader\StatsCollector\StatsCollector;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 */
class BatchSizeCalculator
{
    /** @var StatsCollector */
    private $statsCollector;
    /** @var array */
    private $getterMap;

    /**
     * BatchSizeCalculator constructor.
     * @param StatsCollector $statsCollector
     */
    public function __construct(
        StatsCollector $statsCollector,
        $resizeTube,
        $uploadTube,
        $doneTube,
        $failedTube
    ) {
        $this->statsCollector = $statsCollector;
        
        $this->getterMap = [
            $resizeTube => 'getResizeCount',
            $uploadTube => 'getUploadCount',
            $doneTube   => 'getDoneCount',
            $failedTube => 'getFailedCount'
        ];
    }

    /**
     * Calculates which amount of job script should process given amount in tube and parameter proposed by user
     * @param string $tube
     * @param int $proposedNumber
     * @return int
     */
    public function calculate($tube, $proposedNumber)
    {
        $stats = $this->statsCollector->get();
        $getter = $this->getterMap[$tube];
        $total = $stats->$getter();

        return $proposedNumber ? min($proposedNumber, $total) : $total;
    }
}
