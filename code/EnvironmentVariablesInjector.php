<?php

namespace ImageUploader;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 */
class EnvironmentVariablesInjector
{
    private $variableList = [
        'DROPBOX_ACCESS_TOKEN'       => 'dropbox.access_token',
        'DROPBOX_APPLICATION_SECRET' => 'dropbox.app_secret',
        'RESIZED_DIRECTORY'          => 'resized_directory',
        'BEANSTALKD_HOST'            => 'beanstalkd.host'
    ];
    
    public function inject(ContainerInterface $container)
    {
        foreach ($this->variableList as $env => $configKey) {
            if (isset($_ENV[$env])) {
                $container->setParameter($configKey, $_ENV[$env]);
            }
        }
    }
}
