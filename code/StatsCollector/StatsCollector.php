<?php

namespace ImageUploader\StatsCollector;

use ImageUploader\Model\Stats;
use Pheanstalk\Exception\ServerException;
use Pheanstalk\PheanstalkInterface;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 */
class StatsCollector
{
    /** @var PheanstalkInterface */
    private $pheanstalk;
    /** @var string */
    private $resizeTube;
    /** @var string */
    private $uploadTube;
    /** @var string */
    private $doneTube;
    /** @var string */
    private $failedTube;

    /**
     * @param PheanstalkInterface $pheanstalk
     * @param string $resizeTube
     * @param string $uploadTube
     * @param string $doneTube
     * @param string $failedTube
     */
    public function __construct(
        PheanstalkInterface $pheanstalk,
        $resizeTube,
        $uploadTube,
        $doneTube,
        $failedTube
    ) {
        $this->pheanstalk = $pheanstalk;
        $this->resizeTube = $resizeTube;
        $this->uploadTube = $uploadTube;
        $this->doneTube = $doneTube;
        $this->failedTube = $failedTube;
    }

    /**
     * Gets job counts from application tubes
     * @return Stats
     */
    public function get()
    {
        $resizeCount = $this->tryCollect($this->resizeTube);
        $uploadCount = $this->tryCollect($this->uploadTube);
        $doneCount = $this->tryCollect($this->doneTube);
        $failedCount = $this->tryCollect($this->failedTube);
        
        return new Stats($resizeCount, $uploadCount, $doneCount, $failedCount);
    }

    /**
     * Gets job count for given tube name. Returns 0 in case tube not (yet) exits.
     * @param string $tubeName
     * @return int
     */
    private function tryCollect($tubeName)
    {
        try {
            $stats = $this->pheanstalk->statsTube($tubeName);
            
            return (int) $stats['total-jobs'];
        } catch (ServerException $e) {
        }

        return 0;
    }
}
