<?php

namespace ImageUploader;

use ImageUploader\Command\HelpCommand;
use ImageUploader\Command\ResizeCommand;
use ImageUploader\Command\RetryCommand;
use ImageUploader\Command\ScheduleCommand;
use ImageUploader\Command\StatusCommand;
use ImageUploader\Command\UploadCommand;

require_once __DIR__ . '/vendor/autoload.php';

$timezone = getenv('DATE_TIMEZONE') ?: 'Europe/Berlin';

date_default_timezone_set($timezone);

$helpCommand = new HelpCommand();
$application = new Application();
$application->add($helpCommand);
$application->add(new ScheduleCommand());
$application->add(new ResizeCommand());
$application->add(new UploadCommand());
$application->add(new StatusCommand());
$application->add(new RetryCommand());
$application->setDefaultCommand('help');
$application->run();
