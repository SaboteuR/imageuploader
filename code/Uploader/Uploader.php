<?php

namespace ImageUploader\Uploader;

use ImageUploader\BatchSize\BatchSizeCalculator;
use ImageUploader\Task\BatchTask;
use League\Flysystem\Filesystem;
use Pheanstalk\Job;
use Pheanstalk\PheanstalkInterface;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 */
class Uploader extends BatchTask
{
    /** @var Filesystem */
    private $filesystem;
    /** @var string */
    private $doneTube;
    /** @var string */
    private $failedTube;

    /**
     * @param Filesystem $filesystem
     * @param PheanstalkInterface $pheanstalk
     * @param BatchSizeCalculator $batchSizeCalculator
     * @param string $uploadTube
     * @param string $doneTube
     * @param string $failedTube
     */
    public function __construct(
        Filesystem $filesystem,
        PheanstalkInterface $pheanstalk,
        BatchSizeCalculator $batchSizeCalculator,
        $uploadTube,
        $doneTube,
        $failedTube
    ) {
        $this->filesystem = $filesystem;
        $this->doneTube = $doneTube;
        $this->failedTube = $failedTube;

        parent::__construct($batchSizeCalculator, $pheanstalk, $uploadTube);
    }

    /**
     * @inheritdoc
     */
    protected function processSingleJob()
    {
        /** @var Job $job */
        $job = $this->pheanstalk->watch($this->sourceTube)
            ->ignore('default')
            ->reserve();
        $jobInfo = json_decode($job->getData(), true);

        try {
            $stream = fopen($jobInfo['resizedPath'], 'r');
            $this->filesystem->writeStream('images/' . $jobInfo['filename'], $stream);
            $this->addJob($this->doneTube, $jobInfo);
            
            unlink($jobInfo['sourcePath']);
            unlink($jobInfo['resizedPath']);
        } catch (\Exception $e) {
            $jobInfo['failure'] = $e->getMessage();
            $this->addJob($this->failedTube, $jobInfo);
        }

        $this->pheanstalk->delete($job);
    }
}
