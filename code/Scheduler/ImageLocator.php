<?php

namespace ImageUploader\Scheduler;

use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 */
class ImageLocator
{
    /** @var string */
    private $filterRegex;
    /** @var Finder */
    private $finder;

    /**
     * @param array $allowedExtensions
     */
    public function __construct(array $allowedExtensions)
    {
        $this->filterRegex = $this->compileFilterRegex($allowedExtensions);
        $this->finder = new Finder();
    }

    /**
     * Locates all applicable images within given path
     * @param string $path
     * @return SplFileInfo[]
     */
    public function locate($path)
    {
        $iterator = $this->finder
            ->ignoreUnreadableDirs()->files()->in($path)->name($this->filterRegex);

        return $iterator;
    }

    /**
     * Compiles allowed extension list to regex used by file finder
     * @param array $allowedExtensions
     * @return string
     */
    private function compileFilterRegex(array $allowedExtensions)
    {
        return sprintf('/\.(%s)$/', implode('|', $allowedExtensions));
    }
}
