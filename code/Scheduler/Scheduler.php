<?php

namespace ImageUploader\Scheduler;

use Pheanstalk\PheanstalkInterface;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 */
class Scheduler
{
    /** @var ImageLocator */
    private $imageLocator;
    /** @var PheanstalkInterface */
    private $pheanstalk;
    /** @var string */
    private $resizeTube;

    /**
     * @param ImageLocator $imageLocator
     * @param PheanstalkInterface $pheanstalk
     * @param string $resizeTube
     */
    public function __construct(
        ImageLocator $imageLocator,
        PheanstalkInterface $pheanstalk,
        $resizeTube
    ) {
        $this->imageLocator = $imageLocator;
        $this->pheanstalk = $pheanstalk;
        $this->resizeTube = $resizeTube;
    }

    /**
     * Schedules images from given path for processing
     * @param string $path
     */
    public function schedule($path)
    {
        $foundImages = $this->imageLocator->locate($path);

        foreach ($foundImages as $image) {
            $payload = json_encode([
                'sourcePath' => $image->getRealPath(),
                'filename'   => $image->getFilename()
            ]);
            $this->pheanstalk->useTube($this->resizeTube)->put($payload);
        }
    }
}
