<?php

namespace ImageUploader\Model;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 */
class Stats
{
    private $resizeCount = 0;
    private $uploadCount = 0;
    private $doneCount = 0;
    private $failedCount = 0;

    /**
     * @param int $resizeCount
     * @param int $uploadCount
     * @param int $doneCount
     * @param int $failedCount
     */
    public function __construct($resizeCount, $uploadCount, $doneCount, $failedCount)
    {
        $this->resizeCount = $resizeCount;
        $this->uploadCount = $uploadCount;
        $this->doneCount = $doneCount;
        $this->failedCount = $failedCount;
    }


    /**
     * @return int
     */
    public function getResizeCount()
    {
        return $this->resizeCount;
    }

    /**
     * @return int
     */
    public function getUploadCount()
    {
        return $this->uploadCount;
    }

    /**
     * @return int
     */
    public function getDoneCount()
    {
        return $this->doneCount;
    }

    /**
     * @return int
     */
    public function getFailedCount()
    {
        return $this->failedCount;
    }
}
