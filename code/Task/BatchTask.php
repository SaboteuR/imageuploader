<?php

namespace ImageUploader\Task;

use ImageUploader\BatchSize\BatchSizeCalculator;
use Pheanstalk\PheanstalkInterface;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 */
abstract class BatchTask
{
    /** @var BatchSizeCalculator */
    private $batchSizeCalculator;
    /** @var PheanstalkInterface */
    protected $pheanstalk;
    /** @var string */
    protected $sourceTube;

    /**
     * @param BatchSizeCalculator $batchSizeCalculator
     * @param PheanstalkInterface $pheanstalk
     * @param string $sourceTube
     */
    public function __construct(BatchSizeCalculator $batchSizeCalculator, PheanstalkInterface $pheanstalk, $sourceTube)
    {
        $this->batchSizeCalculator = $batchSizeCalculator;
        $this->pheanstalk = $pheanstalk;
        $this->sourceTube = $sourceTube;
    }

    /**
     * Attempts to process given number of entries from queue
     * @param $suggestedNumOfJobs
     */
    public function run($suggestedNumOfJobs)
    {
        $jobSize = $this->batchSizeCalculator->calculate($this->sourceTube, $suggestedNumOfJobs);
        while ($jobSize > 0) {
            $this->processSingleJob();
            
            $jobSize--;
        }
    }

    /**
     * @param string $tube
     * @param array $jobInfo
     */
    protected function addJob($tube, array $jobInfo)
    {
        $payload = json_encode($jobInfo);
        $this->pheanstalk->useTube($tube)->put($payload);
    }

    /**
     * Process single job from main queue of current task
     * 
     * @return void
     */
    abstract protected function processSingleJob();
}
