<?php

namespace ImageUploader\Retrier;

use ImageUploader\BatchSize\BatchSizeCalculator;
use ImageUploader\Task\BatchTask;
use Pheanstalk\Job;
use Pheanstalk\PheanstalkInterface;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 */
class Retrier extends BatchTask
{
    /** @var string */
    private $resizeTube;
    /** @var string */
    private $failTube;

    /**
     * @param PheanstalkInterface $pheanstalk
     * @param BatchSizeCalculator $batchSizeCalculator
     * @param string $resizeTube
     * @param string $failTube
     */
    public function __construct(
        PheanstalkInterface $pheanstalk,
        BatchSizeCalculator $batchSizeCalculator,
        $resizeTube,
        $failTube
    ) {
        $this->resizeTube = $resizeTube;
        $this->failTube = $failTube;

        parent::__construct($batchSizeCalculator, $pheanstalk, $failTube);
    }

    /**
     * Returns one job from failed queue to resize list
     */
    protected function processSingleJob()
    {
        /** @var Job $job */
        $job = $this->pheanstalk->watch($this->sourceTube)
            ->ignore('default')
            ->reserve();

        $jobInfo = json_decode($job->getData(), true);
        try {
            unset($jobInfo['failure']);
            $this->addJob($this->resizeTube, $jobInfo);
        } catch (\Exception $e) {
            //noop
        }
        $this->pheanstalk->delete($job);
    }
}
