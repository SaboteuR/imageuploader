<?php

namespace ImageUploader;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\Console\Application as ConsoleApplication;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 */
class Application extends ConsoleApplication
{
    /** @var ContainerBuilder */
    private $container;

    public function __construct($name = 'UNKNOWN', $version = 'UNKNOWN')
    {
        $this->container = new ContainerBuilder();
        $loader = new XmlFileLoader($this->container, new FileLocator(__DIR__));
        $loader->load('services.xml');

        $envInjector = new EnvironmentVariablesInjector();
        $envInjector->inject($this->container);

        $this->container->compile();

        parent::__construct($name, $version);
    }

    public function add(Command $command)
    {
        if ($command instanceof ContainerAwareInterface) {
            $command->setContainer($this->container);
        }

        return parent::add($command);
    }
}
