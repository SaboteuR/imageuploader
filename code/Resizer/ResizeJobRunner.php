<?php

namespace ImageUploader\Resizer;

use Intervention\Image\ImageManager;
use Pheanstalk\Job;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 */
class ResizeJobRunner
{
    const TARGET_WIDTH = 640;
    const TARGET_HEIGHT = 640;
    const RESIZING_ANCHOR = 'center';
    const BACKFILL_COLOR = '#FFFFFF';
    
    /** @var ImageManager */
    private $manager;
    /** @var string */
    private $resizeTargetDirectory;

    /**
     * ResizeJobRunner constructor.
     * @param ImageManager $manager
     * @param string $resizeTargetDirectory
     */
    public function __construct(ImageManager $manager, $resizeTargetDirectory)
    {
        $this->manager = $manager;
        $this->resizeTargetDirectory = $resizeTargetDirectory;
        
        if (!file_exists($resizeTargetDirectory)) {
            mkdir($resizeTargetDirectory);
        }
    }

    /**
     * 
     * @param Job $job
     * @return string path to resized image
     */
    public function run(Job $job)
    {
        $jobParameters = json_decode($job->getData(), true);
        $source = $jobParameters['sourcePath'];
        $filename = $jobParameters['filename'];
        $target = sprintf('%s/%s', $this->resizeTargetDirectory, $filename);
        
        $this->manager
            ->make($source)
            ->resize(self::TARGET_WIDTH, self::TARGET_HEIGHT, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })
            ->resizeCanvas(self::TARGET_WIDTH, self::TARGET_HEIGHT, self::RESIZING_ANCHOR, false, self::BACKFILL_COLOR)
            ->save($target)
        ;
        
        return $target;
    }
}
