<?php

namespace ImageUploader\Resizer;

use ImageUploader\BatchSize\BatchSizeCalculator;
use ImageUploader\Task\BatchTask;
use Pheanstalk\Job;
use Pheanstalk\PheanstalkInterface;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 */
class Resizer extends BatchTask
{
    /** @var ResizeJobRunner */
    private $jobRunner;
    /** @var string */
    private $uploadTube;
    /** @var string */
    private $failedTube;

    /**
     * @param ResizeJobRunner $jobRunner
     * @param PheanstalkInterface $pheanstalk
     * @param BatchSizeCalculator $batchSizeCalculator
     * @param string $resizeTube
     * @param string $uploadTube
     * @param string $failedTube
     */
    public function __construct(
        ResizeJobRunner $jobRunner,
        PheanstalkInterface $pheanstalk,
        BatchSizeCalculator $batchSizeCalculator,
        $resizeTube,
        $uploadTube,
        $failedTube
    ) {
        $this->jobRunner = $jobRunner;
        $this->pheanstalk = $pheanstalk;
        $this->resizeTube = $resizeTube;
        $this->uploadTube = $uploadTube;
        $this->failedTube = $failedTube;

        parent::__construct($batchSizeCalculator, $pheanstalk, $resizeTube);
    }

    /**
     * @inheritdoc
     */
    protected function processSingleJob()
    {
        /** @var Job $job */
        $job = $this->pheanstalk->watch($this->sourceTube)
            ->ignore('default')
            ->reserve();
        $jobInfo = json_decode($job->getData(), true);

        try {
            $resized = $this->jobRunner->run($job);

            $jobInfo['resizedPath'] = $resized;
            $this->addJob($this->uploadTube, $jobInfo);
        } catch (\Exception $e) {
            $jobInfo['failure'] = $e->getMessage();
            $this->addJob($this->failedTube, $jobInfo);
        }
        $this->pheanstalk->delete($job);
    }
}
