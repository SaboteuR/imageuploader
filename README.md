## Installation and usage

1. Install [docker](https://www.docker.com/);
2. set DROPBOX_ACCESS_TOKEN and DROPBOX_APPLICATION_SECRET in docker-compose.yml file;
3. place images in `images` subfolder of root of this project. Either you can configure mount source and target by edition of docker-compose.yml;
4. run application with `docker-compose run application help`. Instead of help you can use `schedule`, `resize`, `upload`, `status` 
and `retry` as was requested by task specification. That will build container when you run it for the first time;

## Getting required credentials

1. Create application in [dropbox developer center](https://www.dropbox.com/developers/apps) to get app secret;
2. create upload token in [api explorer](https://dropbox.github.io/dropbox-api-v2-explorer/#files_upload).

## Keep in mind 

1. Fails when try to upload when file with same name already exists in dropbox;
2. despite it allows to schedule random directory, the this directory should be mounted to docker first;
3. docker env is not being cleaned after GD compilation, so container size is slightly bigger;
4. this supposed to be run in docker, so set environment variables instead of editing the config.
